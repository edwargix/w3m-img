#!/bin/sh

# Requires:
# w3m with image support (may be included or need "w3m-img" package depending on distro)
# a terminal with w3m support (urxvt works. Others may or may not, try your luck)

if [ $# -eq 0 ]; then
    echo "No inputs."
    exit 1
fi

HTML=''
for image in $@; do
    HTML=$HTML"<img src=\"$image\">\n"
done
echo -e $HTML | w3m -T text/html
