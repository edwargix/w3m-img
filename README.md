## w3m-img

Use `w3m` to view images in your terminal!

Needs `w3m` with image support (may be included or need a package like `w3m-img`
depending on your distro), a terminal that supports `w3m` images (`urxvt` is the
standard).

## Usage

`w3m-img.sh foo.jpg bar.jpg baz.jpg`

Large images will be stacked; small images will be placed next to each other.
